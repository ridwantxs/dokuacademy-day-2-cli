#!/bin/sh
curdate=`date`
name="$1"
fbname="$2"
linkedinName="$3"
dirname="$name at $curdate"
mkdir -p "$dirname"
cd "$dirname"
mkdir about_me
cd about_me
mkdir personal
cd personal
echo "https://www.facebook.com/$2" > facebook.txt
cd ..
mkdir professional
cd professional
echo "https://www.linkedin.com/in/$3" > linkedin.txt
cd ..
cd ..
mkdir my_friends
cd my_friends
echo `curl "https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt"` > list_of_my_friends.txt
cd ..
mkdir my_system_info
cd my_system_info
echo "My username: `uname`" > about_this_laptop.txt
echo "With host: `uname -a`" >> about_this_laptop.txt
echo "Connection to google:" > internet_connection.txt
echo `ping google.com -c 3` >> internet_connection.txt

